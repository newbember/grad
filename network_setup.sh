!#bin/sh
hostname
echo "-=-=-=-=-=-=-=-=-"
for iname in $(ip a | awk '/state UP/{print $2}')
do
echo "$iname"
ip a | grep -A2 $iname | awk '/inet/{print $2}'
ip a | grep -A2 $iname | awk '/link/{print $2}'
if (grep -A2 $iname | awk '/default/{print $1$3}')
ip r | grep -A2 $iname | awk '/default/{print $1$3}'
fi
ethtool $iname | grep "Speed:"
done
